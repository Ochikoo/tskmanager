import React from 'react';
import ReactDOM from 'react-dom';

function App() {
    return (
        <div className="container">
            I'm an example component!
        </div>
    );
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
